﻿using Mic.VetEducation.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace Mic.VetEducation.Repository.ConsoleTesApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = new StudentRepository();
            var res1 = repo.ReadToList();

            repo.Add(new Student { Id = 7, Name = "B3", Surname = "B3yan" });

            repo.SaveChanges();
            var res2 = repo.ReadToList();
        }

        private static IEnumerable<Student> GetRandomStudent(int count)
        {
            var rnd = new Random();
            for (int i = 1; i < count; i++)
            {
                yield return new Student
                {
                    Name = $"A{i}",
                    Surname = $"A{i}yan"
                };
            }
        }
    }
}
