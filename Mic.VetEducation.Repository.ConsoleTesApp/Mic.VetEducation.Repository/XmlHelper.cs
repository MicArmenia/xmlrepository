﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Mic.VetEducation.Repository
{
    internal static class XmlHelper
    {
        public static List<T> ReadToList<T>(string fleName)
            where T : class, new()
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            using FileStream stream = new FileStream(fleName, FileMode.Open);
            return (List<T>)serializer.Deserialize(stream);
        }

        public static void SaveAs<T>(string fleName, List<T> source)
            where T : class, new()
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            using FileStream stream = new FileStream(fleName, FileMode.Open);
            serializer.Serialize(stream, source);
        }
    }
}
