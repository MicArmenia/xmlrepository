﻿using Mic.VetEducation.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mic.VetEducation.Repository
{
    public class StudentRepository
    {
        private bool _isLoaded;
        private Dictionary<int, Student> _source;

        public StudentRepository() : this("students.xml") { }

        public StudentRepository(string fileName)
        {
            FileName = fileName;
            _source = new Dictionary<int, Student>();
        }

        public string FileName { get; private set; }

        public void Add(Student student)
        {
            _source.Add(student.Id, student);
        }

        public bool Remove(int id)
        {
            return _source.Remove(id);
        }

        //public void Insert(int index, Student student)
        //{
        //    _source.Insert(index, student);
        //}

        public IEnumerable<Student> ReadAll()
        {
            if (!_isLoaded)
            {
                _isLoaded = true;
                try
                {
                    _source = XmlHelper
                        .ReadToList<Student>(FileName)
                        .ToDictionary(p => p.Id);
                }
                catch (Exception ex)
                {
                    //TODO [Log] [Artyom Tonoyan] [28/10/2020]: Write log.
                }
            }

            return _source.Values;
        }

        public List<Student> ReadToList()
        {
            return ReadAll().ToList();
        }

        public void SaveChanges()
        {
            XmlHelper.SaveAs(FileName, _source.Values.ToList());
        }
    }
}
