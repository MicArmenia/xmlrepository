﻿namespace Mic.VetEducation.Repository.Migrations
{
    public static class StudentMigration
    {
        //migrtion number: 1
        public static void AddIdInStudent()
        {
            var repo = new StudentRepository();
            var source = repo.ReadToList();
            for (int i = 0; i < source.Count; i++)
            {
                source[i].Id = i + 1;
            }
            repo.SaveChanges();
        }
    }
}
